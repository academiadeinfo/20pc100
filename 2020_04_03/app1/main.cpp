#include <iostream>

using namespace std;

/// https://tinyurl.com/ai-if-app1
/// ex. 1

int main()
{
    int picioare, ochi, vaci;
    //cout << "Elif și Elsa merg în vacanță la bunici. Pe drum ei văd vaci. Elif se uită pe geamul din stânga și zice că vede x picioare. Elsa se uită pe geamul din dreapta și numără y ochi." << endl;
    cout << "picioare: ";
    cin >> picioare;
    cout << "ochi: ";
    cin >> ochi;

    vaci = picioare / 4   +   ochi / 2;

    cout << "vaci: " << vaci << endl;

    return 0;
}
