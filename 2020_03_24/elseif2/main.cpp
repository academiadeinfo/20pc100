#include <iostream>

using namespace std;

int main()
{
    int n;
    cout << "n = ";
    cin >> n;

    if( n < 0 )
        cout << "negativ" << endl;
    else if(n < 10)
        cout << "mic" << endl;
    else if(n < 1000)
        cout << "mare" << endl;
    else
        cout << "foarte mare" << endl;

    return 0;
}
