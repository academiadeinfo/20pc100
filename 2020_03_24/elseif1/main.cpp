#include <iostream>

using namespace std;

/**
Se citesc două numere naturale a și b.
Să se compare cele două numere.
**/

int main()
{
    int a, b;
    cout << "a = ";
    cin >> a;
    cout << "b = ";
    cin >> b;

    if(a < b) //dacă a este mai mic
        cout << "a < b" << endl;
    else if(a == b) //dacă sunt egale
        cout << "a == b" << endl;
    else ///dacă nu a fost adevărat niciunul din cazurile precedente
        cout << "a > b" << endl;

    return 0;
}
