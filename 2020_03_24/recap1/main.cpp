#include <iostream>

using namespace std;

int main()
{
    ///Se citește un număr natural n. Să se verifice:
    int n;
    cout << "n = ";
    cin >> n;

    ///	1. dacă este divizibil cu 3
    if(n % 3 == 0)
        cout << "n e divizibil cu 3" << endl;
    else
        cout << "n nu este divizibil cu 3" << endl;

    /// 2. dacă ultima cifră este cuprinsă între 3 și 7
    if(3 < n % 10  &&  n % 10 < 7)
        cout << "ucif a lui n este intre 3 si 7" << endl;
    else
        cout << "ucif a lui n nu este intre 3 si 7" << endl;

    ///	3. dacă are exact 3 cifre
    if( 99 < n  and  n < 1000)
        cout << "exact 3 cifre" << endl;
    else
        cout << "nu are exact 3 cifre" << endl;

    /// 4. dacă ultimele 3 cifre sunt în ordine crescătoare
    if(n%10 > n/10%10 && n/10%10 > n/100%10)
        cout << "ultimele 3 cif in ordine crescatoare" << endl;
    else
        cout << "ultimele 3 cif nu sunt in ordine cresc." << endl;

    return 0;
}
