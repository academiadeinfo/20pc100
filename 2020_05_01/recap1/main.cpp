#include <iostream>

using namespace std;

///La ferma lui, John are capre: negre, albe și bălțate
///Se citesc tipuri de capre pana cand
///se introduce cuvantul"gata"
///Câte capre au fost din fiecare?

int main()
{
    string culoare;///ultima culoare citita
    int albe, negre, baltate; ///3 contoare

    ///initializez contoarele cu valoarea 0:
    albe = 0;
    negre = 0;
    baltate = 0;

    while(culoare != "gata") ///cât timp nu am citit "gata"
    {
        cout << "- ";
        cin >> culoare; ///citesc următoarea culoare
        if(culoare == "alba")
            albe++; /// albe = albe + 1; crestem contorul 1
        else if(culoare == "neagra")
            negre++;/// crestem contorul de negre
        else if(culoare == "baltata")
            baltate++;///crestem contorul de baltate
    }
    /// după ce am numărat toate caprele
    ///afișez rezultatul (contoarele)
    cout << endl << "Totaluri: " << endl << endl;
    cout << "albe: " << albe << endl << "negre: " << negre << endl << "baltate: " << baltate<<endl;
    return 0;
}
