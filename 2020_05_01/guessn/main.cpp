#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;

///JOC: ghiceste numărul

int main()
{
    srand(time(NULL));///pregătesc randomul.
    int vieti = 10; ///declar si setez numărul de vieți
    int numar = rand( )%1000; ///declar și setez numărul de ghicit (random intre 0 si 1000)
    bool castigat = false; ///variabilă care spune dacă am câștigat sau nu
    int x; /// ultimul număr citit

    while(vieti > 0 && !castigat) ///cât timp mai am vieți și nu am câștigat încă
    {
        cout << "mai ai: " << vieti << " vieti"<<endl;
        cout << "introduce un numar: ";
        cin >> x; ///citim următorul număr
        if(x < numar)
        {
            cout << "prea mic" << endl;
            vieti--;  /// vieti = vieti - 1
        }
        else if(x > numar)
        {
            cout << "prea mare" << endl;
            vieti--; ///scad o viata
        }
        else
        {
            cout << "perfect";
            castigat = true;
        }
    }

    if(castigat)
        cout << endl <<"BRAVO! ai ghicit! (^_^)"<<endl;
    else
        cout << endl << "Oups, de data asta ai pierdut :P" << endl;

    return 0;
}
