#include <iostream>

using namespace std;

int main()
{
    string creion = ""; ///ultimul creion citit
    int rosii, albastre, verzi, alte;
    rosii = 0;
    albastre = 0;
    verzi = 0;
    alte = 0;
    while(creion != "negru")///cât timp ultimul creion nu a fost negru
    {
        cin >> creion;
        if(creion == "rosu")
            rosii++;
        else if(creion == "albastru")
            albastre++;
        else if(creion == "verde")
            verzi++;
        else ///dacă nu a fost nici una din culorile de mai sus
            alte++; ///cresc contorul de "altele"
    }

    cout << "rosii: " << rosii << endl;
    cout << "albastre: " << albastre << endl;
    cout << "verzi: " << verzi << endl;
    cout << "altele: " << alte << endl;

    cout << endl << "cele mai multe: ";
    if(rosii >= albastre && rosii >= verzi) ///dacă am mai multe rosii
        cout << "rosii ";///afișez rosii
    if(albastre >= rosii && albastre >= verzi) ///dacă am mai multe albastre
        cout << "albastre "; ///afișez albastre
    if(verzi >= rosii && verzi >= albastre) ///idem
        cout << "verzi ";
    cout << endl;

    return 0;
}
