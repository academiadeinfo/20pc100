#include <iostream>

using namespace std;

/**
Golangii, trib mic de înălțime din estul Codarniei, s-au hotărât să creeze o poartă specială la intrarea în ascunzătoarea lor secretă.
Ei vor ca această poartă să fie cât mai mică, dar destul de înaltă încât să încapă toți membrii nobili ai tribului. Înălțimea porții trebuie să fie un număr întreg.
Se citesc înălțimile membrilor nobili (în cm), la sfârșit se introduce 0.
Afișați ce înălțime trebuie să aibă poarta (cm).
*/

int main()
{
    int maxim; ///cel mai mare de până acum
    int inalt; /// ultimul număr citit

    cin >> inalt; ///citesc primul număr
    maxim = inalt; /// maximul este primul număr;

    while(inalt != 0) ///cât timp nu am introdus 0
    {
        cin >> inalt; ///citesc următoarea înălțime
        if(inalt > maxim) ///dacă noul nr e mai mare decât maximul
            maxim = inalt; ///schimb maximul
    }

    cout << "inaltimea portii: " << maxim + 1 << endl; ///afișez rez.


    return 0;
}
