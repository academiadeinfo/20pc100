#include <iostream>

using namespace std;

int main()
{
    int nrafisari = 0;

    while ( nrafisari < 25 ) ///cât timp condiția este adevărată
    {
        ///execută instrucțiunile:
        cout << nrafisari << ". ";
        cout << "Vreau sa invat while-ul" << endl;
        nrafisari = nrafisari + 1;
    }

    ///continua programul

    return 0;
}
