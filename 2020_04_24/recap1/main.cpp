#include <iostream>

using namespace std;

/**
Elif a mâncat x prăjituri, iar Elsa a mâncat y prăjituri.
Se citesc numerele x și y,
Determinați care dintre ei a mâncat mai multe.
*/

int main()
{
    int x, y;
    cout << "Elif: ";
    cin >> x;
    cout << "Elsa: ";
    cin >> y;

    if(x > y)
        cout << "Elif a mancat mai multe" << endl;
    else if(x == y)
        cout << "Au mancat la fel de multe"<<endl;
    else
        cout << "Elsa a mancat mai multe" << endl;

    return 0;
}
