#include <iostream>

using namespace std;

int main()
{
    int x = -1;
    int pare, impare; ///două contoare pentru numere pare și impare.
    pare = 0; ///până acum 0 numere pare
    impare = 0; ///până acum 0 numere impare

    while( x != 0 ) ///cât timp ultimul nr introdus este diferit de 0
    {
        cin >> x; ///citesc următorul număr
        ///afișez dacă numărul citit este par sau impar.
        if(x % 2 == 0)
        {
            cout << "par" << endl;
            pare = pare + 1; ///cresc contorul de numere pare
        }
        else
        {
            cout << "impar" << endl;
            impare = impare + 1; ///cresc contorul de numere impare
        }
    }
    ///la sfârșit afișez contoarele (o singură dată - afară din while)
    cout << endl << "total pare: " << pare <<endl;
    cout << "total impare: " << impare <<endl;
    return 0;
}
