#include <iostream>

using namespace std;

/**
Scrieți un program care citește 2 numere
naturale și afișează minimul dintre ele.
Ex. pentru 7 3 se va afisa minim: 3
*/

int main()
{
    int a, b;///declarăm variabilele
    ///citim datele de intrare
    cout << "introduceti 2 nr: ";
    cin >> a >> b;
    if(a < b) ///dacă primul nr este mai mic
    {
        cout << "minim: " << a << endl;///atunci afișez primul număr
    }
    else ///altfel
    {
        cout << "minim: " << b << endl;///afișez al 2-lea număr
    }
    return 0;
}
