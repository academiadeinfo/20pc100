#include <iostream>
#include <windows.h>

using namespace std;

int main()
{
    /// se citește un număr n, să se facă o numărătoare
    /// inversă de la n la 0 (BOOM)
    int n;
    cin >> n;

    for(int cont = n; cont > 0; cont-- )
    {
        cout << "       " << cont << endl;
        Sleep(1000);
    }
    cout << endl << "    BOOOOM !" << endl;

    return 0;
}
