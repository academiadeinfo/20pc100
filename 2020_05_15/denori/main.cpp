#include <iostream>

using namespace std;


int main()
{
    ///Să se execute ceva de n ori:
    ///ex: să se afișeze de n ori mesajul: merge forul
    int n;
    cin >> n;
    for(int i = 1; i <= n; i++)
    {
        ///facem ceva
        cout << i << ". Merge forul" << endl;
    }

    return 0;
}
