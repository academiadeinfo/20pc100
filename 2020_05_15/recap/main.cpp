#include <iostream>

using namespace std;

int main()
{
    ///Se citesc numerele a si b cu a < b

    int a, b;
    cin >> a >> b;

    ///să se afișeze numerele de la a la b din 2 în 2
    for(int i = a; i <= b; i += 2)
    {
        cout << i << " ";
    }

    cout << endl;

    ///să se afișeze numerele de la b la a
    /// din 3 în 3 descrescător.
    for(int i = b;  i >= a  ;   i -= 3)
    {
        cout << i << " ";
    }
    cout << endl;

    ///provocare: să se afișeze tabla înmulțirii lui a până la 10
    ///  a x 1 = a
    ///  a x 2 = ...
    ///  a x 3 = ...
    /// ......
    ///  a x 10 = ...

    for(int i = 1; i <= 10; i++)
        cout << a << " x " << i << " = " << a*i << endl;

    return 0;
}
