#include <iostream>
#include <windows.h>

using namespace std;

int main()
{
    ///1. Elsa merge cu autobuzul dint-run capăt până în stația centrală.
    /// în fiecare statie urcă x persoane și coboară y persoane
    /// câte persoane sunt in autobuz când coboară Elsa?

    int n; ///numărul de stații.
    int pers; ///numărul de persoane;
    int x, y;

    cout << "cate statii merge Elsa: ";
    cin >> n;
    pers = 0;

    for(int i = 1; i <= n; i++) ///pentru fiecare stație i de la 1 la n
    {
        cout << "statia " << i << ": ";
        cin >> x >> y;
        pers += x;
        pers -= y;
    }

    cout << "la sfarsit: " << pers << " persoane" << endl;

    return 0;
}
