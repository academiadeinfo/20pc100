#include <iostream>

using namespace std;

int main()
{
    for(int i = 1; i <= 10; i++)
        cout << i << " ";

    cout << endl;

    for(int x = 5; x <= 20; x += 3)
    {
        cout << " -> " << x;
    }

    cout << endl;

    /// Vreau să fac ceva de 10 ori:
    for(int i = 1; i <= 10; i++)
    {
        ///fac acel ceva
        cout << i << ". Am inceput forul"<<endl;
    }
    cout << endl;

    ///For descrescător:
    for(int x = 20; x >= 0; x -= 2)
    {
        cout << x << " ";
    }

    cout << endl;


    return 0;
}
