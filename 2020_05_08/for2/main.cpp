#include <iostream>

using namespace std;

/**
Se citesc numerele a și b
    afișati toate numerele dintre a și b.
    (*) nu știu în ce ordine sunt a și b
*/

int main()
{
    int a, b;
    cin >> a >> b;

    for(int nr = a; nr <= b; nr+=1)
    {
        cout << nr << " ";
    }

    cout << endl;

    return 0;
}
