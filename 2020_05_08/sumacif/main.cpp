#include <iostream>

using namespace std;

int main()
{
    int n;
    cin >> n; ///citim numărul

    int suma = 0; ///luam o variabila în care calculăm suma

    while(n != 0) ///cât timp n mai are cifre
    {
        suma += n % 10; ///adăugăm la sumă ultima cifră a lui n
        n /= 10; /// eliminăm ultima cifră a lui n
    }

    cout << "suma cifrelor: " << suma << endl; ///afișez rezultatul

    return 0;
}
