#include <iostream>

using namespace std;

int main()
{
    cout << "--- BANCOMAT ---" << endl << endl;
    int sold = 0; /// la �nceput nu avem bani
    int s;

    while(1)
    {
        string op;
        cout << "introduceti operatia (d, e, v): ";
        cin >> op;
        if(op == "d")
        {
            cout << "suma depusa: ";
            cin >> s;
            sold += s; /// sold = sold + s;
            cout << endl;
        }
        else if (op == "e")
        {
            cout << "suma dorita: ";
            cin >> s;
            if( s <= sold)
            {
                sold -= s; /// sold = sold - s
                cout << "poftiti banii!" << endl << endl;
            }
            else
                cout << "fonduri insuficiente!" << endl << endl;
        }
        else if (op == "v")
            cout << "sold curent: " << sold << endl << endl;
    }

    return 0;
}
